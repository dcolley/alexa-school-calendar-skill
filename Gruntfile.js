var AWS = require('aws-sdk')
AWS.config.region = 'us-west-1'

module.exports = function(grunt) {
	grunt.loadNpmTasks('grunt-aws-lambda');

	grunt.initConfig({

		lambda_invoke: {
			default: {
				options: {
					// Task-specific options go here.
					// event: 'test/event.json',
					// event: 'test/pipers-nodate.json',
					// event: 'test/drchall-today.json',
					// event: 'test/godstowe-today.json',
					// event: 'test/highmarch-hi.json',
					event: 'test/highmarch-today.json',
					region: 'eu-west-1',
				}
			}
		},

		lambda_deploy: {
			default: {
				package: '',
				arn: 'arn:aws:lambda:eu-west-1:515792683057:function:GodstoweSchool',
				options: {
						// Task-specific options go here.
					region: 'eu-west-1',
					accessKeyId: '',
					secretAccessKey: '',
				}
			}
		},

		lambda_package: {
			default: {
				options: {
					// Task-specific options go here.
					region: 'eu-west-1',
				}
			}
		},

	});

	grunt.registerTask('deploy', ['lambda_package', 'lambda_deploy']);

}