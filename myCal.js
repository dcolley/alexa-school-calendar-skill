
// https://icalevents.com/support/documentation/shortcodes/#parameters

var ical = require('ical')

var moment = require('moment-timezone');
moment.locale("en-GB");
var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
var adp = require('amazon-date-parser');

const log = require('lambda-log');
log.config.debug = true;

var states = {
	'UNREGISTERMODE': '_UNREGISTER', // unregister a child, requires confirmation
	'LISTENMODE': '_LISTEN', // unregister a child, requires confirmation
};

// var _day = moment( "2017-W31-WE", "YYYY-" );
// var _day = adp("2017-W31-WE");
// console.log(_day);
// _day = adp("2017-11-12");
// console.log(_day);

function isEmpty(obj) {
  for(var key in obj) {
    if(obj.hasOwnProperty(key))
    	// console.log( '[DEBUG] isEmpty returning false' );
    	// console.log(obj);
      return false;
  }
	// console.log( '[DEBUG] isEmpty returning true' );
  return true;
}

module.exports = {

	htmlEscape: function (str) {
		return str
			.replace(/&/g, '&amp;')
			.replace(/"/g, '&quot;')
			.replace(/'/g, '&#39;')
			.replace(/</g, '&lt;')
			.replace(/>/g, '&gt;');
	},

	htmlUnescape: function(str){
		return str
			.replace(/&quot;/g, '"')
			.replace(/&#39;/g, "'")
			.replace(/&lt;/g, '<')
			.replace(/&gt;/g, '>')
			.replace(/&amp;/g, '&');
	},

	dayName: function( num ) {
		switch( num ) {
			case 1:
				return 'Monday'; break;
			case 2:
				return 'Tuesday'; break;
			case 3:
				return 'Wednesday'; break;
			case 4:
				return 'Thursday'; break;
			case 5:
				return 'Friday'; break;
			case 6:
				return 'Saturday'; break;
			case 7:
				return 'Sunday'; break;
		}
	}, 

	getDayRef: function( day ) {
		var _day = adp( day );
		// console.log(_day);
		_day.startDate = moment.utc(_day.startDate);
		_day.endDate   = moment.utc(_day.endDate);
		// console.log('getDayRef: ');
		// console.log(_day);
		var ret = "";

		var _span = _day.endDate.diff(_day.startDate, 'days') // 1 ..2=weekend?
		// console.log( 'the span is '+_span);

		switch(_span) {
			case 0:
			case 1:
				// a day - continues below
				break;
			case 2:
				// a weekend?
				return 'on the weekend'; break;
			case 6:
			case 7:
				// a week?
			default:
				log.debug("getDayRef: Invalid date range");
				console.log(day);
				throw("Invalid date range");
		}

		var _today = adp( moment().format('YYYY-MM-DDT00:00') );
		_today.startDate = moment.utc(_today.startDate);
		_today.endDate   = moment.utc(_today.endDate);
		// console.log(_today);

		var _diff = _day.endDate.diff(_today.endDate, 'days') // 1
		// console.log( 'the diff is '+_diff);

		switch( true ) {
			case (_diff<0):
				return 'in the past'
				break;
			case (_diff===0): 
				return 'today';
				break;
			case (_diff===1): 
				return 'tomorrow';
				break;
			case (_diff<6):
				return 'on ' + this.dayName( _day.startDate.isoWeekday() );
				break;
			default: // date in the future, return: dayName, day of month
				return 'on ' + this.dayName( _day.startDate.isoWeekday() ) 
							+ ', ' + _day.startDate.format('Do MMMM'); // 1st 2nd ..rd ..th
		};
	},

	getTimeRef: function( date ) {
		var ret = "";
		var mdate = moment( date );
		// midnight
		if( 
			(mdate.hour() == "0" || mdate.hour() =="00") &&
			(mdate.minute() == "0" || mdate.minute() =="00")
		) return ret;

		ret += "at " +mdate.hour();
		if( mdate.minutes() === 0 ) {
			ret +=':00';
		} else if( mdate.minutes() < 10) {
			ret += ':0'+mdate.minutes();
		} else {
			ret += ':'+mdate.minutes();
		}
		return ret;
	},

	parseEventDescription: function( event, config ) {
		// console.log( mapObj );
		var field = (config.fields && config.fields.description) ? config.fields.description : 'description' ;
		// console.log('[DEBUG] using the "'+field+'" field for description: '+event[field]);
		var desc = event[field];
		if( isEmpty( config.map ) ) {
			ret = event[field];
		} else {
			var re = new RegExp(Object.keys(config.map).join("|"),"gi");
			var ret = desc.replace(re, function(matched){
				return config.map[matched];
			});
		}
		return ret;
	},

	getCalendar: function( config, cb ) {
		var _code    = config.code     || 'godstowe';
		var _proto   = config.protocol || "ical";
		var _url     = config.url      || 'http://www.godstowe.org/media/calendar/ical/Calendar';
		var _limit   = config.limit    || 50;
		var _isArray = config.isArray  || false;
		log.debug('myCal.getCalender(): getting calendar data for '+_code);

		// ical.fromURL( _url, {}, cb );

		ical.fromURL( _url, {}, function( err, data ){
			log.debug("myCal.getCalendar(): returns...");
			// console.log(data);

			// console.log('[DEBUG] data type = '+Object.prototype.toString.call( data ));
			var i=1;
			switch( Object.prototype.toString.call( data ) ) {
				case '[object Array]': 

					log.debug('found Array');
					if( !_isArray ) console.warn("[WARN ] found Array in ics, but config says Object!");
					log.debug( 'myCal.getCalendar(): converting to object...');
					var obj = {};
					for( item in data ){
						if( i > _limit ) delete(data[item]);
						else obj[item] = data[item];
						i++;
					};
					data = obj;
					if( i > _limit ) delete(data[item]);
					// console.log(data);
					break;

				case '[object Object]':

					log.debug('found Object');
					if( _isArray ) console.warn("[WARN ] found Object in ics, but config says Array! ");
					// console.log( '[DEBUG] === '+JSON.stringify( data ).substr( 1,2000 ) );
					break;
			}

			i=1;
			var count = {VTIMEZONE: 0, VEVENT:0};
			for( var item in data ) {
				// console.log( '[DEBUG] === '+JSON.stringify(data[item]).substr(1,100) );
				count[data[item].type]++;
				switch( true ) {
					case ( i > _limit ):
						// console.log('doing delete, i = '+i);
						delete( data[item] );
						break;
					case ( data[item].type=="VTIMEZONE" ):
						console.log('found VTIMEZONE, i = '+i);
						break;
					case ( data[item].type=="VEVENT" ):
						data[item].start = moment( data[item].start );
						data[item].end   = moment( data[item].end );
						i++;
						break;
				}
			}
			// console.log( count );

			log.debug("myCal.getCalendar(): i = "+i);
			cb( err, data );
		} );

	},

	/**
	 * moment(start), moment(end), {object of data items}
	 */
	filterCalendarItems: function( start, end, data ) {
		log.debug('filterCalendarItems() start:'+start.format("YYYY-MM-DDTHH:mm:ss")
				+" - end:"+end.format("YYYY-MM-DDTHH:mm:ss"));
		var ret = [];
		start = moment(start);
		end   = moment(end);

		// console.log(data);
		// console.log("filtering "+ data.keys().length +" items");
		for( var item in data ) {
			// console.log("..");

			// data[item].start = moment.parseZone( data[item].start ).utc();
			// data[item].end   = moment.parseZone( data[item].end ).utc();

			// data[item].start = moment( data[item].start );
			// data[item].end   = moment( data[item].end );

			// console.log("item.start: "+data[item].start +" :: "+ start);

			if(
				( start < data[item].start && end > data[item].start  ) 
			||( start < data[item].end   && end > data[item].end  ) 
			)
			ret.push( data[item] );
		}
		if(ret.length > 0) {
			ret.sort(function(a, b){
				// Compare the 2 dates
				if( a.start < b.start ) return -1;
				if( a.start > b.start ) return 1;
				return 0;
			});
		}
		// console.log(ret);
		return ret;
	},

};