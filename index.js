/* eslint-disable  func-names */
/* eslint quote-props: ["error", "consistent"]*/
/**
 * TODO: write a description.. here!
 **/

'use strict';

const Alexa = require('alexa-sdk');
const log = require('lambda-log');
log.config.debug = true;

const myCal = require('./myCal');
const model = require('./model');

var ical = require('ical')
var moment = require('moment-timezone');
moment.locale("en-GB");
var adp = require('amazon-date-parser');

var appConfig = require('./appConfig');

var VoiceLabs = require("voicelabs")('9e3407f0-b3fc-11a7-0c61-0eb19d13e26e');


// SOME UNIT TESTING for the command line =====================================
// var start = '2017-09-24';
// var end   = '2017-09-25';
// myCal.getCalItems({start: start+'T23:59:59', end: end+'T23:59:59'}, function( err, data ){
// 	console.log(data);
// 	if (err) console.log(err);
// 	var strItems = '';
// 	for( var key in data ) {
// 		// console.log( data[key]);
// 		if( data.hasOwnProperty(key) ) {
// 			strItems += myCal.htmlEscape( data[key].description ) 
// 				+ " at " + myCal.getTimeRef( data[key].start ) +", ";
// 		}
// 	};
// 	console.log( myCal.getDayRef( start )+' we have '+strItems );
// });

// console.log(myCal.parseDescription('U12 & U13 U13 & U14, U9 (by appointment) (home) (away)') );
// console.log( myCal.getDayRef('2017-10-18T00:00') );
// console.log( myCal.getDayRef('2015-W49-WE') );
// console.log( myCal.getDayRef('2017-W49') );
// SOME UNIT TESTING for the command line =====================================

const states = {
	'UNREGISTERMODE': '_UNREGISTER', // unregister a child, requires confirmation
	'LISTENMODE': '_LISTEN', // unregister a child, requires confirmation
};

const newSessionHandlers = {
//  'LaunchRequest': function () {
//      this.attributes.speechOutput = this.t('WELCOME_MESSAGE', this.t('SKILL_NAME'));
//      // If the user either does not reply to the welcome message or says something that is not
//      // understood, they will be prompted again with this text.
//      this.attributes.repromptSpeech = this.t('WELCOME_REPROMPT');
//      this.emit(':ask', this.attributes.speechOutput, this.attributes.repromptSpeech);
//  },
	'NewSession': function() {
		// if( !this.event.request || !this.event.request.intent ) {
		// 	console.log('Oops, there is no intent!, event and session follow');
		// 	console.log( this.event );
		// 	console.log( this.session );
		// } else 
		var config = appConfig[this.event.session.application.applicationId];
		VoiceLabs = new VoiceLabs()(config.voicelabs);

		var that = this;
		if(    !( "event" in this ) 
			|| !( "request" in this.event ) 
			|| !( "intent" in this.event.request ) 
			|| this.event.request.intent.name == "NewSession" ) {
			this.response.speak('Welcome to '+config.name+ '! How can I help?')
				.listen('You can say ... what\'s in the calendar today ... or bye to quit.');
			//this.handler.state = states.LISTENMODE;
			VoiceLabs.track(this.event.session, "NewSession", null, ':responseReady', (error, response) => {
				that.emit(':responseReady');
			});

		} else {
			VoiceLabs.track(that.event.session, that.event.request.intent.name, that.event.request.intent.slots, that.event.request.intent.name, (error, response) => {
				that.emit( that.event.request.intent.name );
			});
		}
	},

	'CalendarItemsIntent': function( ) {
		var that = this;
		var date = this.event.request.intent.slots.date.value || 
			moment().format('YYYY-MM-DD');

		// unit test
		// date = moment().format("YYYY-MM-DD");
		log.debug("calendar items for date: "+date);

		var adate = adp( date );
		// console.log("[DEBUG] "+adate);
		var config = appConfig[this.event.session.application.applicationId];
		VoiceLabs = new VoiceLabs()(config.voicelabs);

		// this.attributes.calendar = "blah";
		// this.attributes.refreshTime = moment();
		// this.attributes.refreshTime.subtract(60, 'minutes');

		model.getCalendarItems( config, function( err, items ){
			if( err ) log.error( err );
			if( items ) {
				var data = myCal.filterCalendarItems( moment.utc(adate.startDate), moment.utc(adate.endDate), items );

				// console.log("filtered items follow");
				// console.log(data);

				if( data.length ==0 ) {
					that.response.speak( 'there is nothing in the calendar '+myCal.getDayRef(date) );
				} else {
					var strItems = data.length;
					strItems += (data.length==1) ? " item, " : " items, ";

					// data is an array [], not an object...!
					// console.log(data);

					for( var key in data ) {
						// console.log( data[key]);
						if( data.hasOwnProperty(key) ) {
							strItems += 
								myCal.getTimeRef( data[key].start ) + ", "
								+ myCal.htmlEscape( myCal.parseEventDescription( data[key], config ) ) 
								+ ", ";
						}
					};
					that.response.speak( myCal.getDayRef( date )+', we have '+strItems );
				};

			} else {
				that.response.speak( 'Error: I could not get data for the calendar' );
			};
			VoiceLabs.track(that.event.session, that.event.request.intent.name, that.event.request.intent.slots, ':responseReady', (error, response) => {
				that.emit(':responseReady');
			});
		});

	},

	'AMAZON.HelpIntent': function () {
		var config = appConfig[this.event.session.application.applicationId];
		VoiceLabs = new VoiceLabs()(config.voicelabs);
//      this.attributes.speechOutput = this.t('HELP_MESSAGE');
//      this.attributes.repromptSpeech = this.t('HELP_REPROMPT');
//      this.emit(':ask', this.attributes.speechOutput, this.attributes.repromptSpeech);
		this.response.speak('Welcome to '+config.name+ '! You can say ... what\'s in the calendar today ... or bye to quit. How can I help?')
			.listen('You can say ... what\'s in the calendar today ... or bye to quit. How can I help?');
		//this.handler.state = states.LISTENMODE;
		this.emit(':responseReady');
	},
//  'AMAZON.RepeatIntent': function () {
//      this.emit(':ask', this.attributes.speechOutput, this.attributes.repromptSpeech);
//  },
	'AMAZON.StopIntent': function () {
		this.emit('SessionEndedRequest');
	},
	'AMAZON.CancelIntent': function () {
		this.emit('SessionEndedRequest');
	},
	'SessionEndedRequest': function () {
		this.emit(':tell', this.t('STOP_MESSAGE') );
	},
//  'Unhandled': function () {
//      this.attributes.speechOutput = this.t('HELP_MESSAGE');
//      this.attributes.repromptSpeech = this.t('HELP_REPROMPT');
//      this.emit(':ask', this.attributes.speechOutput, this.attributes.repromptSpeech);
//  },
};

var listenModeHandlers = Alexa.CreateStateHandler( states.LISTENMODE, {
	'NewSession': function () {
		this.handler.state = '';
		this.emit('NewSession');        // delegate to newSessionHandlers
	},
	'CalendarItemsIntent': function() {
		this.emit('CalendarItemsIntent');  // delegate
	},
	'ChildListIntent': function() {
		var user = this.event.session.user;
		var that = this;
		var config = appConfig[this.event.session.application.applicationId];
		VoiceLabs = new VoiceLabs()(config.voicelabs);
		model.getChild( user.userId, null, function(err, data) {
			if(err) {
				that.response.speak( err );
			} else {
				if( !data.Items || data.Items.size === 0 ) {
					that.response.speak( 'there are no children registered.' );
				} else if( data.Items.size === 0 ) {
					that.response.speak( data.Items.childName.S+' is registered.' );
				} else {
					var kids = "";
					// console.log('data.Items follow:');
					// console.log(data.Items);
					for(var idx=0; idx < data.Items.length; idx++) {
						if(idx === 0) {
							kids += data.Items[idx].childName.S;
						}
						else if( idx === data.Items.length - 1 ){
							kids += " and " +data.Items[idx].childName.S;
						} else {
							kids += ", " +data.Items[idx].childName.S;
						}
					}
					that.response.speak( kids +' are registered.' );
				}
			}
			VoiceLabs.track(that.event.session, that.event.request.intent.name, that.event.request.intent.slots, ':responseReady', (error, response) => {
				that.emit(':responseReady');
			});
		});
	},
	'ChildRegister': function() {
	   // console.log( this.event.session.user );
		var user = this.event.session.user;
		var config = appConfig[this.event.session.application.applicationId];
		VoiceLabs = new VoiceLabs()(config.voicelabs);
		
		const childNameSlot = this.event.request.intent.slots.childName;
		let childName;
		if ( childNameSlot && childNameSlot.value ) {
			childName = childNameSlot.value;
		}

		this.attributes.childName = childName;
		var that = this;
		model.registerChild( user.userId, childName, function( err, data ) {
			if( err ) { 
				log.error(err);
			 //   that.emit( ':tell', err, null );
				that.response.speak( err ).listen('Please say that again?');
			} else {
			 //   that.emit( ':tell', 'Success! '+childName+' is now registered.', null );
				that.response.speak( childName+' is now registered.' );
			}
			that.emit(':responseReady');
		});
		// this.emit(':responseReady');
	},
	'ChildUnregister': function () {
		var user = this.event.session.user;
		var config = appConfig[this.event.session.application.applicationId];
		VoiceLabs = new VoiceLabs()(config.voicelabs);

		const childNameSlot = this.event.request.intent.slots.childName;
		if ( childNameSlot && childNameSlot.value ) {
			this.attributes.childName = childNameSlot.value;
		}
		this.handler.state = states.UNREGISTERMODE;
		var that = this;
//      this.emit( 'ChildUnregisterConfirm', this.attributes.childName );
		model.getChild( user.userId, this.attributes.childName, function(err, data) {
			if( err ) {
				that.response.speak(err);
			} else {
				if( data.Item && data.Item.childName.S == that.attributes.childName ) {
					that.response.speak("Are you sure you want to unregister "+that.attributes.childName+'?').listen('Please say yes or no');
				} else {
					that.handler.state = states.LISTENMODE;
					that.response.speak(that.attributes.childName+" is not registered");
					that.attributes.childName = '';
				}
			}
			that.emit(':responseReady');
		});
	},
	'AMAZON.HelpIntent': function () {
		log.debug('AMAZON.HelpIntent');
		this.emit('AMAZON.HelpIntent');
	},
	"AMAZON.StopIntent": function() {
		log.debug("STOPINTENT");
		this.response.speak("Listen mode stopped, Goodbye!");  
		this.emit(':responseReady');  
	},
	"AMAZON.CancelIntent": function() {
		log.debug("CANCELINTENT");
		this.response.speak("Listen mode cancelled, Goodbye!");  
		this.emit(':responseReady');  
	},
	'SessionEndedRequest': function () {
		log.debug("SESSIONENDEDREQUEST");
		//this.attributes['endedSessionCount'] += 1;
		this.response.speak("Listen mode session ended, Goodbye!");  
		this.emit(':responseReady');
	},
	'Unhandled': function() {
		log.debug("UNHANDLED");
		var message = 'Godstowe Calendar. You can ask me, what\'s on friday?, or what\'s in the calendar today?';
		this.response.speak(message);
		// VoiceLabs.track(this.event.session, "Unhandled", null, message, (error, response) => {
			this.emit(':responseReady');
		// });
	}

});

var unregisterModeHandlers = Alexa.CreateStateHandler( states.UNREGISTERMODE, {
	'NewSession': function () {
		this.emit('NewSession'); // Uses the handler in newSessionHandlers
	},
	'ChildUnregister': function () {
		this.emit('ChildUnregister');
//     // var user = this.event.session.user;
//      this.attributes.childName = this.event.request.intent.slots.childName.value;
//         this.response.speak('Do you really want to unregister '+this.attributes.childName+'?')
//             .listen('Say yes to delete all child-related data or no to cancel.');
//         this.emit(':responseReady');
	},
	'AMAZON.HelpIntent': function () {
		this.emit('AMAZON.HelpIntent');
	},
	'AMAZON.YesIntent': function() {
		var user = this.event.session.user;
		var childName = this.attributes.childName;
		this.handler.state = states.LISTENMODE;
		var that = this;
		model.unregisterChild( user.userId, childName, function(err, data) {
			if(err) { 
				log.error(err); 
				that.response.speak( err );
			} else {
				that.response.speak( 'Ok! '+childName+' has been unregistered.' );
			}
			that.emit(':responseReady');
		});
	},
	'AMAZON.NoIntent': function() {
		log.debug("NOINTENT");
		this.handler.state = states.LISTENMODE;
		this.response.speak('Ok, going back to listen mode' );
		this.emit(':responseReady');        
	},
	"AMAZON.StopIntent": function() {
	  this.response.speak("Stop requested, Goodbye!");  
	  this.emit(':responseReady');
	},
	"AMAZON.CancelIntent": function() {
		this.response.speak("Your request was cancelled, Goodbye!");  
		this.emit(':responseReady');  
	},
	'SessionEndedRequest': function () {
		log.debug('session ended!');
		//this.attributes['endedSessionCount'] += 1;
		this.response.speak("Session ended, Goodbye!");  
		this.emit(':responseReady');
	},
	'Unhandled': function() {
		log.debug("[DEBUG] UNHANDLED");
		var message = 'Please confirm by saying yes or no';
		this.response.speak(message);
		this.emit(':responseReady');        
	}
	
});

exports.handler = function( event, context ) {
	// // set some optional metadata to be included in all logs (this is an overkill example) 
 //    log.config.meta.event = event;
 //    // add additional tags to all logs 
 //    log.config.tags.push(event.env);

	log.debug(JSON.stringify(event));
	// if( !(event.request && event.request.locale) ) event.request = { locale: 'en-GB' };
	// console.log(JSON.stringify(context));
	const alexa = Alexa.handler( event, context );

	if( !appConfig.hasOwnProperty(event.session.application.applicationId ) )
		throw("Invalid application: "+event.session.application.applicationId );

	alexa.appId = event.session.application.applicationId;

	// To enable string internationalization (i18n) features, set a resources object.
//  alexa.resources = languageStrings;

	alexa.dynamoDBTableName = "sessions";
	alexa.registerHandlers( newSessionHandlers, listenModeHandlers, unregisterModeHandlers );
	alexa.execute();
};
