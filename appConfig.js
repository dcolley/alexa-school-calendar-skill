var unders = {
	'U7, U8 & U9': 'Under 7, 8 & 9',
	'U8, U9 & U10': 'Under 8, 9 & 10',
	'U9, U10 & U11': 'Under 9, 10 & 11',
	'U10, U11 & U12': 'Under 10, 11 & 12',
	'U11, U12 & U13': 'Under 11, 12 & 13',
	// match 2
	'U7 & U8': 'Under 7 & 8',
	'U8 & U9': 'Under 8 & 9',
	'U9 & U10': 'Under 9 & 10',
	'U10 & U11': 'Under 10 & 11',
	'U11 & U12': 'Under 11 & 12',
	'U12 & U13': 'Under 12 & 13',
	// match 1
	'U7': 'Under 7',
	'U8': 'Under 8',
	'U10': 'Under 10',
	'U11': 'Under 11',
	'U12': 'Under 12',
	"U13": "under 13 ",
	"U14": "under 14 ",
	"U15": "under 15 ",
	"U16": "under 16 ",
	"U17": "under 17 ",
	"U18": "under 18 ",		
};
var years = {
	// match 3
	'Y1, Y2 & Y3': 'Year 1, 2 & 3',
	'Y2, Y3 & Y4': 'Year 2, 3 & 4',
	'Y3, Y4 & Y5': 'Year 3, 4 & 5',
	'Y4, Y5 & Y6': 'Year 4, 5 & 6',
	'Y5, Y6 & Y7': 'Year 5, 6 & 7',
	'Y6, Y7 & Y8': 'Year 6, 7 & 8',
	'Y7, Y8 & Y9': 'Year 7, 8 & 9',
	'Y8, Y9 & Y10': 'Year 8, 9 & 10',
	'Y9, Y10 & Y11': 'Year 9, 10 & 11',
	'Y10, Y11 & Y12': 'Year 10, 11 & 12',
	'Y11, Y12 & Y13': 'Year 11, 12 & 13',
	// match 2
	'Y1 & Y2'  : 'Year 1 & 2',
	'Y2 & Y3'  : 'Year 2 & 3',
	'Y3 & Y4'  : 'Year 3 & 4',
	'Y4 & Y5'  : 'Year 4 & 5',
	'Y5 & Y6'  : 'Year 5 & 6',
	'Y6 & Y7'  : 'Year 6 & 7',
	'Y7 & Y8'  : 'Year 7 & 8',
	'Y8 & Y9'  : 'Year 8 & 9',
	'Y9 & Y10' : 'Year 9 & 10',
	'Y10 & Y11': 'Year 10 & 11',
	'Y11 & Y12': 'Year 11 & 12',
	'Y12 & Y13': 'Year 12 & 13',
	// ranges
	'Y1-Y3': 'Years 1 to 3',
	'Y2-Y4': 'Years 2 to 4',
	'Y3-Y5': 'Years 3 to 5',
	'Y4-Y6': 'Years 4 to 6',
	'Y5-Y7': 'Years 5 to 7',
	'Y10-Y13': 'Years 10 to 13',
	// match 1
	'Y1': 'Year 1',
	'Y2': 'Year 2',
	'Y3': 'Year 3',
	'Y4': 'Year 4',
	'Y5': 'Year 5',
	'Y6': 'Year 6',
	'Y7': 'Year 7',
	'Y8': 'Year 8',
	'Y10': 'Year 10',
	'Y11': 'Year 11',
	'Y12': 'Year 12',
	'Y13': 'Year 13',
};
var romans = {
	// ' I ' : ' 1 ', // ... should we replace a lonely I...
	'II'  : ' 2 ',
	'III' : ' 3 ',
	'IV'  : ' 4 ',
	' V ' : ' 5 ', // only by itself
	'VI'  :  '6',
	'VII' :  '7',
	'VIII':  '8',
	'IX'  :  '9',
	' X ' : ' 10 ', // only by itself
	'XI'  :  '11',
	"XII" :  "12",
	"XIII":  "13",
	"XIV" :  "14",
	"XV"  :  "15",
	"XVI" :  "16",
};


// list of apps and related config
module.exports = {

	"beckyhigh": {
		code: "beckyhigh",
		name: "Becky High",
		protocol: "ics",
		url: "http://beaconsfieldhigh.bucks.sch.uk/ical.ics",
		map: Object.assign(
			{}, 
			years
		),
			// "by appointment":' ',
			// "\(by appointment\)":'-',
			// "\(home\)":' ',
			// "\(away\)":' ',
		// },
		voicelabs: "b89fd0b0-b4d3-11a7-173a-0e61e4c2ee12",
	},

	"amzn1.ask.skill.4beaf687-1997-4cd8-b901-465485b6c702": {
		code: "drchall",
		name: "Dr. Challoners Calendar",
		protocol: 'ics',
		url: 'https://www.google.com/calendar/ical/challoners.org_1e3c7g4qn1kic52usnlbrn63ts%40group.calendar.google.com/public/basic.ics',
		// Google calendar returns 1700-ish records...!
		limit: 100,
		isArray: true, // returns [] instead of {}
		fields: {
			description: 'summary',
		},
		map: Object.assign(
			{}, 
			romans
		),
		voicelabs: "cf231170-b5a6-11a7-173a-0e61e4c2ee12",
	},

	"amzn1.ask.skill.c66d027d-2187-4ddd-9f95-fdd0cab57288": {
		code: "godstowe",
		name: "Godstowe Calendar",
		protocol: 'ical',
		url: 'http://www.godstowe.org/media/calendar/ical/Calendar',
		map: Object.assign(
			{}, 
			unders
		),
			// "by appointment":' ',
			// "\(by appointment\)":'-',
			// "\(home\)":' ',
			// "\(away\)":' ',
		voicelabs: '9e3407f0-b3fc-11a7-0c61-0eb19d13e26e',
	},


	"amzn1.ask.skill.78fd9c52-a3ab-4d05-a851-2a37c1f31ce0": {
		code: "highmarch",
		name: "High March Calendar",
		protocol: 'ical',
		url: 'https://calendar.google.com/calendar/ical/22m7eh0f534o4i5v8pa1mseqoo%40group.calendar.google.com/public/basic.ics',
		limit: 100,
		fields: {
			description: 'summary',
		},
		map: {},
		voicelabs: '0e280880-b5a7-11a7-173a-0e61e4c2ee12',
	},


	"amzn1.ask.skill.6f7cad44-bded-40e5-9a76-5adb5d534276": {
		code: "pipers",
		name: "Pipers Corner Calendar",
		protocol: 'ical',
		url: 'http://www.piperscorner.co.uk/media/calendar/ical/Calendar',
		// limit: 100,
		map: Object.assign(
			{},
			romans, 
			unders
		),
		voicelabs: "b89fd0b0-b4d3-11a7-173a-0e61e4c2ee12",
	},

}