# Alexa School Calendar Skill

## Amazon Alexa Skill

This is the 'frontend', please see the [speechAssets](https://bitbucket.org/dcolley/alexa-school-calendar-skill/src/master/speechAssets) directory.

## Amazon Lambda Function

This function is the 'backend' to an Alexa App.

## Amazon DynamoDB

Tables required for

### session
Holds the session variables

### calendar
Holds school calendar info. Data is cached for 5 minutes so that subsequent requests do not cause load on the school server.

# Privacy Policy

This service uses information that is publicly available - published on school websites.
No personal information is stored.

## Support calendar formats

### ical

For a list of supported/configured schools, please see the [appConfig.js](https://bitbucket.org/dcolley/alexa-school-calendar-skill/src/master/appConfig.js) file

### other
For schools that do not provide iCal data, we need to look at web-scraping...

# appConfig

The skill behaviour can be modified in the appConfig, e.g.:
```json
"amzn1.ask.skill.c66d027d-2187-4ddd-9f95-fdd0cab57288": {
		code: "godstowe",
		name: "Godstowe Calendar",
		protocol: 'ical',
		url: 'http://www.godstowe.org/media/calendar/ical/Calendar',
		map: {
			// match 3
			'U7, U8 & U9': 'Under 7, 8 & 9',
			'U8, U9 & U10': 'Under 8, 9 & 10',
			'U9, U10 & U11': 'Under 9, 10 & 11',
			'U10, U11 & U12': 'Under 10, 11 & 12',
			'U11, U12 & U13': 'Under 11, 12 & 13',
			// match 2
			'U7 & U8': 'Under 7 & 8',
			'U8 & U9': 'Under 8 & 9',
			'U9 & U10': 'Under 9 & 10',
			'U10 & U11': 'Under 10 & 11',
			'U11 & U12': 'Under 11 & 12',
			'U12 & U13': 'Under 12 & 13',
			// match 1
			'U7': 'Under 7',
			'U8': 'Under 8',
			'U10': 'Under 10',
			'U11': 'Under 11',
			'U12': 'Under 12',
			'U13': 'Under 13',
			// "by appointment":' ',
			// "\(by appointment\)":'-',
			// "\(home\)":' ',
			// "\(away\)":' ',
		},
		voicelabs: '9e3407f0-b3fc-11a7-0c61-0eb19d13e26e',
	},
```

## String: appId

This is the Amazon Alexa (frontend) Application Id. 
e.g. `"amzn1.ask.skill.c66d027d-2187-4ddd-9f95-fdd0cab57288"`

A separate application (Skill) should exist for each school. This Skill will hold the branding and invocation word for each school.
When the backend service is executed, it will pick up the config relative to the `event.session.application.applicationId`.

## String: code

The short-code of the school, e.g. `godstowe`.

## String: name

The spoken name of the school, e.g. `Godstowe Calendar`.

## String: protocol

The protocol of the school's published calendar, e.g. `ical`.
Currently not used, it's here for future calendar formats.

## String: url

The url of the school's published calendar, e.g. `http://<school>/calendar`.
Most calendars are published with `webcal://<school>/calander`. We simply use http instead of webcal.

## Array: map

An array of text substitutions.
Many school calendars use appreviations that don't 'speak' well. E.g. U10 is pronounced 'you ten'.


# Credits

*So many people* have been kind enough to share their experience and methods.
Please refer the [CREDITS](https://bitbucket.org/dcolley/alexa-school-calendar-skill/src/master/CREDITS) file for a list of resources I found useful.


# License

MIT

# Calendars

http://www.godstowe.org/media/calendar

http://www.piperscorner.co.uk/media/calendar


