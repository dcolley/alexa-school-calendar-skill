
const AWS = require('aws-sdk');
var db = new AWS.DynamoDB({
    "apiVersion": "2012-08-10",
    "region": "eu-west-1"
});
const myCal = require('./myCal');
const moment = require('moment-timezone');
moment.locale("en-GB");

const log = require('lambda-log');
log.config.debug = true;

module.exports = {

	/**
	 * @config[] = appConfig[ appid ]
	 */
	getCalendarItems: function( config, cb ) {
		log.debug('model.getCalendarItems() Searching for calendar: '+ config.code );
		var key = { code: { S:config.code } };
		params = { TableName: "calendar", Key: key };
		var that = this;
		db.getItem( params, function( err, data ){
			if( err ) cb(err, null);
			// console.log("model.getCalendarItems() data follows:");
			// console.log(data);

			if( data.Item 
				&& moment.duration( moment().diff(moment(data.Item.updateTime.S)) ).asMinutes() < 1
			 ) {

				log.debug('model.getCalendarItems() returning cached data: age='+moment.duration( moment().diff(moment(data.Item.updateTime.S)) ).asMinutes()+" mins" );
				cb( null, JSON.parse( data.Item.items.S ) );

			} else {

				log.debug('model.getCalendarItems() cache expired, fetching new data');
				myCal.getCalendar( config, function( err, items ) {
					if( err ) cb(err, null);
					that.setCalendar( {code: config.code, items: items}, function(err, cal){
						if( err ) log.log(err);
						cb( null, items );
					});
				});

			}
		} );
	},

	/**
	 * calendar: { code, items, updateTime }
	 * cb: promise
	 */
	setCalendar: function( calendar, cb ){
		log.debug('model.setCalendar() firing...');
		// console.log( calendar );
		var cal = {
			updateTime: { S:moment().format("YYYY-MM-DDThh:mm:ss") },
			code:  { S: calendar.code },
			items: { S: JSON.stringify( calendar.items ) },
		}
		var params = { TableName: "calendar", Item: cal };
		db.putItem( params, cb );
	},
	
	// getChild: function( userId, childName, cb) {
	// 	var params;
	// 	if( childName ) {
	// 		log.debug('model.getChild() Searching for '+ childName );
	// 		var key = { userId: { S:userId }, childName: { S:childName } };
	// 		params = { TableName: "child", Key: key };
	// 		db.getItem( params, cb );
	// 	} else {
	// 		log.debug('Searching for all children of '+userId);
	// 		params = {
	// 			"TableName": "child",
	// 			"KeyConditionExpression": "userId = :v1",
	// 			"ExpressionAttributeValues": {
	// 				":v1": {"S": userId}
	// 			},
	// 		};
	// 		db.query( params, function( err, data ){
	// 			if( err ) log.log(err);
	// 			cb( err, data );
	// 		});
	// 	}
	// },

	// registerChild: function( userId, childName, cb ) {
	// 	getChild( userId, childName, function( err, data ) {
	// 		if( err ) {
	// 			 cb( 'Error: '+err, null );
	// 		} else {
	// 			var key = { userId: { S:userId }, childName: { S:childName } };
	// 			var params = { TableName: "child", Item: key };
	// 			if( data.Item && data.Item.childName.S == childName ) {
	// 				cb( childName+' is already registered', null );
	// 			} else {
	// 				db.putItem( params, cb );
	// 			}
	// 		}
	// 	});
	// },

	// unregisterChild: function( userId, childName, cb ) {
	// 	var key = { userId: { S:userId }, childName: { S:childName } };
	// 	var params = { TableName: "child", Key: key };
	// 	getChild( userId, childName, function( err, data ) {
	// 		if( err || !data.Item ) {
	// 			cb( childName+' is not registered.', null );    
	// 		} else {
	// 			db.deleteItem( params, function( err, data ) {
	// 				if (err) {
	// 					//console.log("Error " + err);
	// 					cb(err, null);
	// 				} else {
	// 					//console.log("Success " + data);
	// 					cb(null, data);
	// 				}
	// 			});
	// 		}
	// 	});
	// },

} // end file
